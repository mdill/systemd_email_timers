# Systemd E-mail Timer

## Purpose
This is a pair of simple scripts which will set up timers to E-mail a device's
IP address when booted, as well as any time the IP address changes.

The boot scripts will send an E-mail 30 seconds after the device has completed
booting, and the regular scripts will compare the current IP address every
5 minutes, to assure that it hasn't changed -- if it has changed, it will send
a new E-mail with the new IP address.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/setup_email_timers.git
    cd ~/systemd_email_timers/

## First thing's first...
In order to use these scripts, you will first have to modify a few files.

First, it is recommended that you find your wireless device with `ip a`.  You
will then need to put that wireless device in place of `wlan0` in the following
files:

    1. address.service (line 3)
    2. boot_address.service (line 3)
    3. address.sh (line 10)
    4. boot_address.sh (line 6)

Second, you will need to change `email_address` to your actual E-mail address
in two files:

    1. address.sh (line 4)
    2. boot_address.sh (line 4)

It's also recommended that you change `<device>` in both of those files (line 3)
to the actual device name of the machine these files will be going on.

## Installing and enabling
Once that is all done, you will need to make the `setup.sh` file executable:

    chmod +x setup.sh

Now, you can run `./setup.sh` and enter your password.  This setup shell script
will copy the Systemd files into the correct location, and enable them upon
reboot.  You will need to enter your admin password, as your Systemd files are
located in `/etc/systemd/system/` which requires root privileges to
add/edit/remove any files.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/systemd_email_timers/src/8338575412d31958865ad4b02395998ac48bf887/LICENSE.txt?at=master) file for
details.

