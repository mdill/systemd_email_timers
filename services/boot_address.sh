#!/bin/sh

SUBJ="Here is your new <device> address"
EMAIL="email_address@gmail.com"

echo $(ip -4 addr show dev wlan0) | mail -Ssendwait -s "$SUBJ" $EMAIL

