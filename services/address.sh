#!/bin/sh

SUBJ="Your <device> has a new IP"
EMAIL="email_address@gmail.com"

ip1=""
ip2=""

read ip1 < ip.txt
ip2=$(ip -4 addr show dev wlan0)

if [ "$ip1" = "$ip2" ]
then
  exit
else
  echo "$ip2" > ip.txt
  echo "$ip2" | mail -Ssendwait -s "$SUBJ" $EMAIL
  exit
fi

